NS4=IE4=IE5=IE=NS6=0;NS=1;doc=document;ly=null;
if (doc.layers) {NS4=1;ly=doc.layers;}
else if (doc.all) {IE=1;NS=0;ly=doc.all; IE5=(doc.recalc)?1:0; IE4=1-IE5; }
else {NS6=1; ly=doc.getElementsByTagName("*");}
win=window;

function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function swapImage(arg, over)
{
	if (document.images && (preloadFlag == true))
	{
		var divname = 'box' + arg;
		var imgname = 'link' + arg;
		var img = ly[imgname];
		var div = (NS4)?ly[divname]:ly[divname].style;
		if (over)
		{
			if (div.display == "none")
				img.src = "themes/adc/plus-over.gif";
			else
				img.src = "themes/adc/minus-over.gif";
		}
		else
		{
			if (div.display == "none")
				img.src = "themes/adc/plus.gif";
			else
				img.src = "themes/adc/minus.gif";
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		plusImgLoader =			newImage("themes/adc/plus.gif");
		plusImgOverLoader =		newImage("themes/adc/plus-over.gif");
		minusImgLoader =		newImage("themes/adc/minus.gif");
		minusImgOverLoader =	newImage("themes/adc/minus-over.gif");
		preloadFlag = true;
	}
}

function toggle(arg)
{
	if (document.images && (preloadFlag == true))
	{
		var divname = 'box' + arg;
		var imgname = 'link' + arg;
		var img = ly[imgname];
		var div = (NS4)?ly[divname]:ly[divname].style;
		if (div.display != "none")
		{
			div.display = "none";
			img.src = "themes/adc/plus.gif";
		}
		else
		{
			div.display = "block";
			img.src = "themes/adc/minus.gif";
		}
	}
}

