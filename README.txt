adc.theme
---------
Description:  Theme for Drupal 4.1.0 (and probably CVS too).
              Relatively clean CSS approach, making colour
              changes quite easy (although you'll need to edit
              the rollover expand/collapse graphics).

              Please see CVS for a screenshot.

     Author:  Alastair Maw (www.almaw.com)

    Bugs to:  drupal-adc-theme[at]almaw.com

     Blocks:  Left and Right, can rollup (using CSS+JavaScript).
              Rollups known to work on:
                IE 5.5 Win
                IE 6.0 Win
                Mozilla 1.3+


To install:
-----------

Installation is as usual for Drupal themes. If you know what you're doing,
you can skip this bit. :)

Download the tar.gz from Drupal.org, copy it into your themes/ directory
under Drupal's root, and then go:

tar xzvf adc.tar.gz

This will unpack the theme into themes/adc/

Enable the theme as usual under Drupal's admin menus.


Limitations/Issues/TODO:
------------------------
Blocks don't look nice if you don't have at least one block in each of the
left and right columns.

Could use work pulling HTML out of adc.theme and into a separate include file.

Rollups probably don't work in Opera 6 (and probably quite a few other
browsers too).

Internet Explorer has issues with table width="100%" elements embedded in the
main content div. This causes the content to appear below all the blocks, with
dead space in the main panel. This can be fixed by removing all the
width="100%" tags from any tables in modules (or changing them to 95%). A
better Internet Explorer work-around is in the works, but if you can assist
with fixing this, I won't turn you down...